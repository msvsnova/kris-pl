alter table aaorgs add v_pl_list_io char(3) NULL
go
update aaorgs set v_pl_list_io='IP1'
go
alter table ttorgs add v_pl_list_io char(3) NULL
go
update ttorgs set v_pl_list_io='IP1'
go
alter table iiradn add prosjek3b_io decimal(15,2)
go
update iiradn set prosjek3b_io=0
go

alter table aazara add ind_nema_ovrhe char(1) NULL
go
update aazara set ind_nema_ovrhe='N'
go
alter table ttzara add ind_nema_ovrhe char(1) NULL
go
update ttzara set ind_nema_ovrhe='N'
go
alter table vrsati add ind_nema_ovrhe char(1) NULL
go
update vrsati set ind_nema_ovrhe='N'
go
