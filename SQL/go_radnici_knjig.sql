/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_aago_radnici
	(
	pl_knjigovodstvo char(12) NULL,
	cradnik char(6) NOT NULL,
	gmru char(8) NOT NULL,
	v_pl_godobr_radopc smallint NOT NULL,
	v_pl_godobr_spri decimal(6, 2) NOT NULL,
	v_pl_godobr_olakmax decimal(15, 2) NOT NULL,
	v_pl_godobr_iposn decimal(15, 2) NOT NULL,
	v_pl_godobr_iporez decimal(15, 2) NOT NULL,
	v_pl_godobr_iprir decimal(15, 2) NOT NULL,
	v_pl_godobr_aposn decimal(15, 2) NOT NULL,
	v_pl_godobr_aporez decimal(15, 2) NOT NULL,
	v_pl_godobr_aprir decimal(15, 2) NOT NULL,
	v_pl_godobr_indgo char(1) NOT NULL,
	v_pl_godobr_mje_br smallint NOT NULL,
	v_pl_godobr_sum_mj smallint NOT NULL,
	v_pl_godobr_adoho decimal(15, 2) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_aago_radnici SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.aago_radnici)
	 EXEC('INSERT INTO dbo.Tmp_aago_radnici (cradnik, gmru, v_pl_godobr_radopc, v_pl_godobr_spri, v_pl_godobr_olakmax, v_pl_godobr_iposn, v_pl_godobr_iporez, v_pl_godobr_iprir, v_pl_godobr_aposn, v_pl_godobr_aporez, v_pl_godobr_aprir, v_pl_godobr_indgo, v_pl_godobr_mje_br, v_pl_godobr_sum_mj, v_pl_godobr_adoho)
		SELECT cradnik, gmru, v_pl_godobr_radopc, v_pl_godobr_spri, v_pl_godobr_olakmax, v_pl_godobr_iposn, v_pl_godobr_iporez, v_pl_godobr_iprir, v_pl_godobr_aposn, v_pl_godobr_aporez, v_pl_godobr_aprir, v_pl_godobr_indgo, v_pl_godobr_mje_br, v_pl_godobr_sum_mj, v_pl_godobr_adoho FROM dbo.aago_radnici WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.aago_radnici
GO
EXECUTE sp_rename N'dbo.Tmp_aago_radnici', N'aago_radnici', 'OBJECT' 
GO
CREATE UNIQUE CLUSTERED INDEX ixmccyaa ON dbo.aago_radnici
	(
	cradnik,
	gmru
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_go_radnici
	(
	pl_knjigovodstvo char(12) NULL,
	cradnik char(6) NOT NULL,
	v_pl_godobr_radopc smallint NOT NULL,
	v_pl_godobr_spri decimal(6, 2) NOT NULL,
	v_pl_godobr_olakmax decimal(15, 2) NOT NULL,
	v_pl_godobr_iposn decimal(15, 2) NOT NULL,
	v_pl_godobr_iporez decimal(15, 2) NOT NULL,
	v_pl_godobr_iprir decimal(15, 2) NOT NULL,
	v_pl_godobr_aposn decimal(15, 2) NOT NULL,
	v_pl_godobr_aporez decimal(15, 2) NOT NULL,
	v_pl_godobr_aprir decimal(15, 2) NOT NULL,
	v_pl_godobr_indgo char(1) NOT NULL,
	v_pl_godobr_mje_br smallint NOT NULL,
	v_pl_godobr_sum_mj smallint NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_go_radnici SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.go_radnici)
	 EXEC('INSERT INTO dbo.Tmp_go_radnici (cradnik, v_pl_godobr_radopc, v_pl_godobr_spri, v_pl_godobr_olakmax, v_pl_godobr_iposn, v_pl_godobr_iporez, v_pl_godobr_iprir, v_pl_godobr_aposn, v_pl_godobr_aporez, v_pl_godobr_aprir, v_pl_godobr_indgo, v_pl_godobr_mje_br, v_pl_godobr_sum_mj)
		SELECT cradnik, v_pl_godobr_radopc, v_pl_godobr_spri, v_pl_godobr_olakmax, v_pl_godobr_iposn, v_pl_godobr_iporez, v_pl_godobr_iprir, v_pl_godobr_aposn, v_pl_godobr_aporez, v_pl_godobr_aprir, v_pl_godobr_indgo, v_pl_godobr_mje_br, v_pl_godobr_sum_mj FROM dbo.go_radnici WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.go_radnici
GO
EXECUTE sp_rename N'dbo.Tmp_go_radnici', N'go_radnici', 'OBJECT' 
GO
CREATE UNIQUE CLUSTERED INDEX ix4ujiaa ON dbo.go_radnici
	(
	cradnik
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_go_olakmax
	(
	pl_knjigovodstvo char(12) NULL,
	cradnik char(6) NOT NULL,
	v_pl_godobr_mj smallint NOT NULL,
	v_pl_godobr_olak decimal(15, 2) NOT NULL,
	v_pl_godobr_posn decimal(15, 2) NOT NULL,
	v_pl_godobr_porez decimal(15, 2) NOT NULL,
	v_pl_godobr_prir decimal(15, 2) NOT NULL,
	v_pl_godobr_dohomj decimal(15, 2) NOT NULL,
	v_pl_godobr_sprimj decimal(6, 2) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_go_olakmax SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.go_olakmax)
	 EXEC('INSERT INTO dbo.Tmp_go_olakmax (cradnik, v_pl_godobr_mj, v_pl_godobr_olak, v_pl_godobr_posn, v_pl_godobr_porez, v_pl_godobr_prir, v_pl_godobr_dohomj, v_pl_godobr_sprimj)
		SELECT cradnik, v_pl_godobr_mj, v_pl_godobr_olak, v_pl_godobr_posn, v_pl_godobr_porez, v_pl_godobr_prir, v_pl_godobr_dohomj, v_pl_godobr_sprimj FROM dbo.go_olakmax WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.go_olakmax
GO
EXECUTE sp_rename N'dbo.Tmp_go_olakmax', N'go_olakmax', 'OBJECT' 
GO
CREATE UNIQUE CLUSTERED INDEX ix8rjiaa ON dbo.go_olakmax
	(
	cradnik,
	v_pl_godobr_mj
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT