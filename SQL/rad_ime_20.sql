
alter table iiradn add iimin_osn_dop decimal(15,2) NULL
go
update iiradn set iimin_osn_dop=0
go
alter table epdv_mp alter column epdv_ime char(20)
go
alter table plM4 alter column rad_ime char(20)
go
alter table aazregos alter column rad_ime char(20)
go