alter table ah_ttradn add at_isplata datetime NULL
go
alter table ah_aaradn add aa_isplata datetime NULL
go
update ah_ttradn set at_isplata = at_datisp
go
update ah_aaradn set aa_isplata = aa_datisp
go


alter table iiradn add iimin_osn_dop decimal(15,2) NULL
go
update iiradn set iimin_osn_dop=0
go