#------------------------------------------------------------------------------
#
# SuperNova APPLICATION - PL - MAKEFILE
# By FIST -  A.I. 10.08.1999
#
#------------------------------------------------------------------------------

# Compiler options
#------------------------------------------------------------------------------
## CC          = start /w c:\snova\sncd\nova
CC          = start /w c:\vscd\bin\nova


CFLAGS      = -w -ereadfgl
#TD			= T:\pl\ 
TD			= x:\pl\ 
## TD			= c:\kris\pl\ 
YTD			= Y:\NewVer.RAR\pl\ 
AD          = Y:\pl\ 
COMD        = .\lib\plcommon.fle 

PLFILES  =  $(TD)addef_no.fle $(TD)pl_ident.fle $(TD)pl_start.fle $(TD)pl_rev.fle \
   $(TD)fastmr.fle    $(TD)fastosn.fle  $(TD)findpar.fle  $(TD)pl_tuneRS.fle  \
   $(TD)l_kredit.fle  $(TD)l_odbici.fle $(TD)l_potpli.fle $(TD)l_rekban.fle   \
   $(TD)l_uk_izn.fle  $(TD)pl.fle       $(TD)netobruto.fle $(TD)pl_pk_go.fle  \
   $(TD)pl_aobr.fle   $(TD)pl_arhiv.fle $(TD)pl_avirm.fle $(TD)pl_choj.fle    \
   $(TD)pl_disk.fle   $(TD)pl_dmds.fle  $(TD)pl_er.fle    $(TD)pl_fix.fle     \
   $(TD)pl_flds.fle   $(TD)pl_fonds.fle $(TD)pl_getos.fle $(TD)pl_getpr.fle   \
   $(TD)pl_grkr.fle   $(TD)pl_gruid.fle $(TD)pl_id.fle    $(TD)pl_iorgs2.fle      \
   $(TD)pl_iizr.fle   $(TD)pl_iizr2.fle $(TD)pl_ikred.fle $(TD)pl_inior.fle   \
   $(TD)pl_inizr.fle  $(TD)pl_iorgs.fle $(TD)pl_ipor.fle  $(TD)pl_iradn.fle   \
   $(TD)pl_ispld.fle  $(TD)pl_isplo.fle $(TD)pl_ispls.fle $(TD)pl_ispmj.fle   \
   $(TD)pl_istkr.fle  $(TD)pl_izvkr.fle $(TD)pl_keakt.fle $(TD)pl_kebod.fle   \
   $(TD)pl_keimp.fle  $(TD)pl_keola.fle $(TD)pl_keopc.fle $(TD)pl_keorg.fle   \
   $(TD)pl_kerm.fle   $(TD)pl_kesat.fle $(TD)pl_kestz.fle $(TD)pl_kezar.fle   \
   $(TD)pl_klsql.fle  $(TD)pl_ler1.fle  $(TD)pl_lgrid.fle $(TD)pl_lgrkr.fle   \
   $(TD)pl_liip.fle   $(TD)pl_lirad.fle $(TD)pl_lismj.fle $(TD)pl_lispl.fle   \
   $(TD)pl_liste.fle  $(TD)pl_lopri.fle $(TD)pl_lpor.fle  $(TD)pl_lprro.fle   \
   $(TD)pl_lvirm.fle  $(TD)pl_lvro.fle  $(TD)pl_lvrs.fle  $(TD)pl_lzpr.fle    \
   $(TD)pl_m4.fle     $(TD)pl_mds.fle   $(TD)pl_minra.fle $(TD)pl_msgs.fle    \
   $(TD)pl_naco.fle   $(TD)pl_obrkr.fle $(TD)pl_obrnw.fle $(TD)pl_obros.fle   \
   $(TD)pl_obrpr.fle  $(TD)pl_obrtm.fle $(TD)pl_obrz.fle  $(TD)pl_ojbrw.fle   \
   $(TD)pl_pk.fle     $(TD)pl_plraz.fle $(TD)pl_por.fle   $(TD)pl_prmr.fle    \
   $(TD)pl_prpov.fle  $(TD)pl_rispa.fle $(TD)pl_rispl.fle $(TD)pl_rnup.fle    \
   $(TD)pl_roisp.fle  $(TD)pl_stzar.fle $(TD)pl_svikr.fle $(TD)pl_virm.fle    \
   $(TD)pl_virmd.fle  $(TD)pl_virms.fle $(TD)pl_vispc.fle $(TD)pl_vro.fle     \
   $(TD)pl_vrsat.fle  $(TD)pl_zbrek.fle $(TD)pl_zim.fle   $(TD)pl_zimtr.fle   \
   $(TD)ramp.fle      $(TD)pl_dd.fle    $(TD)pldbutil.fle $(TD)pl_plista.fle  \
   $(TD)pl_lovir.fle  $(TD)pl_mjizv.fle $(TD)pl_vrtro.fle $(TD)pl_lrbn.fle    \
   $(TD)lisplpbz.fle  $(TD)pl_nalup.fle $(TD)pl_a_dd.fle  $(TD)l_id_pl_old.fle \
   $(TD)l_listpbz.fle $(TD)pltms_dd.fle $(TD)l_altir.fle  $(TD)pl_godobr.fle   \
   $(TD)l_altik.fle   $(TD)l_ikred.fle  $(TD)pl_rad1.fle  $(TD)l_aakred.fle   \
   $(TD)pl_lvp_h.fle  $(TD)pl_lm4.fle   $(TD)l_ist_kr.fle $(TD)aazregos.fle   \
   $(TD)m4regob.fle   $(TD)regbrM4.fle  $(TD)aazregos.fle $(TD)aasregos.fle   \
   $(TD)l_agerror.fle $(TD)pl_makeRS.fle $(TD)pl_fileRS.fle $(TD)pl_kl3reg.fle \
   $(TD)pl_spl.fle    $(TD)pl_idx.fle  $(TD)lpl_olak.fle $(TD)id_pl.fle      \
   $(TD)l_id_pl.fle   $(TD)lrek_m4.fle  $(TD)pl_fixbod.fle $(TD)m4_rekap.fle  

PLFILES2  =     $(TD)lid_pl_opc.fle $(TD)l_godobr.fle $(TD)l_go_opc.fle $(TD)pl_fileRSm.fle \
   $(TD)lr_osnmio.fle $(TD)pbz_lvrad.fle $(TD)godbruto.fle $(TD)lpl_stzar.fle \
   $(TD)bol_odjel.fle $(TD)l_bol_odjel.fle $(TD)reiizara.fle $(TD)lpl_harac.fle \
   $(TD)l_harac.fle   $(TD)aa_lprro.fle $(TD)pl_aalzpr.fle  $(TD)lpl_tipor.fle   \
   $(TD)pl_id_mp.fle  $(TD)banke.fle    $(TD)l_xii_kartica.fle $(TD)pl_storno.fle \
   $(TD)l_allkred.fle $(TD)shknj_ah.fle $(TD)go_olakmax.fle $(TD)pl_klista5.fle \
   $(TD)l_iiz_joppd.fle $(TD)pl_godobr2.fle $(TD)pl_to_gk.fle $(TD)l_preg_mr.fle \
   $(TD)sepa_vrop.fle $(TD)l_rad1g.fle  $(TD)periodicni.fle $(TD)l_per_place.fle \
   $(TD)l_gmz1_radn.fle $(TD)l_gmz2_radn.fle $(TD)aarekban.fle $(TD)pl_history.fle \
   $(TD)l_history.fle $(TD)l_gmz1_flukt.fle $(TD)l_gmz2_flukt.fle $(TD)iizara_nb.fle


AHFILES  =  $(TD)ah_dd.fle $(TD)ah_radnici.fle $(TD)ah_ugo.fle $(TD)ah_koefs.fle \
            $(TD)ah_a_dd.fle $(TD)ah_calc.fle $(TD)ah_virm.fle $(TD)lah_kliste.fle \
            $(TD)ah_allarh.fle $(TD)ah_idd.fle $(TD)lah_listic.fle $(TD)lah_idd.fle \
            $(TD)lah_iddgod.fle $(TD)ah_fileRSm.fle $(TD)obrz_id1.fle \
            $(TD)lah_harac.fle $(TD)ah_troskovi.fle $(TD)ah_virmd.fle $(TD)hpah_virm.fle 

COMMON   = ah_dd.ls ah_ddx.ls banke.ls pl_avirm.ls pl_dd.ls pl_ddx.ls pl_flds.ls \
           pl_getpr.ls pl_virm.ls pl_virms.ls pl_vispc.ls pl_vrtro.ls  \
           ramp.ls pltms_dd.ls 



all      : $(PLFILES) $(PLFILES2) $(AHFILES) 


ah_dd.ls:  ah_dd.lgc
    $(CC) $(CFLAGS) appl=ah_dd dotfle=.\lib\pl_dd.fle 
    @echo " ">ah_dd.ls

ah_ddx.ls:  ah_ddx.lgc
    $(CC) $(CFLAGS) appl=ah_ddx dotfle=.\lib\pl_dd.fle 
    @echo " ">ah_ddx.ls

banke.ls:  banke.lgc
    $(CC) $(CFLAGS) appl=banke dotfle=$(COMD)
    @echo " ">banke.ls

pl_avirm.ls:  pl_avirm.lgc
    $(CC) $(CFLAGS) appl=pl_avirm dotfle=$(COMD)
    @echo " ">pl_avirm.ls

pl_dd.ls:  pl_dd.lgc
    $(CC) $(CFLAGS) appl=pl_dd dotfle=.\lib\pl_dd.fle 
    @echo " ">pl_dd.ls

pl_ddx.ls:  pl_ddx.lgc
    $(CC) $(CFLAGS) appl=pl_ddx dotfle=.\lib\pl_dd.fle 
    @echo " ">pl_ddx.ls

pl_flds.ls:  pl_flds.lgc
    $(CC) $(CFLAGS) appl=pl_flds dotfle=.\lib\pl_dd.fle 
    @echo " ">pl_flds.ls

pl_getpr.ls:  pl_getpr.lgc
    $(CC) $(CFLAGS) appl=pl_getpr dotfle=$(COMD)
    @echo " ">pl_getpr.ls

ramp.ls:  ramp.lgc
    $(CC) $(CFLAGS) appl=ramp dotfle=$(COMD)
    @echo " ">ramp.ls

pl_in.ls:  pl_in.lgc
    $(CC) $(CFLAGS) appl=pl_in dotfle=$(COMD)
    @echo " ">pl_in.ls

pl_virm.ls:  pl_virm.lgc
    $(CC) $(CFLAGS) appl=pl_virm dotfle=$(COMD)
    @echo " ">pl_virm.ls

pl_virms.ls:  pl_virms.lgc
    $(CC) $(CFLAGS) appl=pl_virms dotfle=$(COMD)
    @echo " ">pl_virms.ls

pl_vispc.ls:  pl_vispc.lgc
    $(CC) $(CFLAGS) appl=pl_vispc dotfle=$(COMD)
    @echo " ">pl_vispc.ls

pl_vrtro.ls:  pl_vrtro.lgc
    $(CC) $(CFLAGS) appl=pl_vrtro dotfle=$(COMD)
    @echo " ">pl_vrtro.ls

pltms_dd.ls:  pltms_dd.lgc
    $(CC) $(CFLAGS) appl=pltms_dd dotfle=$(COMD)
    @echo " ">pltms_dd.ls





$(TD)pl_dd.fle:   pl_dd.lgc
 $(CC) $(CFLAGS) appl=pl_dd dotfle=$(TD)pl_dd.fle
 $(CC) -w genddx ddfile=pl_dd
 $(CC) $(CFLAGS) appl=pl_ddx dotfle=$(TD)pl_dd.fle

$(TD)addef_no.fle:   addef_no.lgc
 $(CC) $(CFLAGS) appl=addef_no dotfle=$(TD)addef_no.fle

$(TD)dbrw.fle:   dbrw.lgc
 $(CC) $(CFLAGS) appl=dbrw dotfle=$(TD)dbrw.fle

$(TD)fastmr.fle:   fastmr.lgc
 $(CC) $(CFLAGS) appl=fastmr dotfle=$(TD)fastmr.fle

$(TD)fastosn.fle:   fastosn.lgc
 $(CC) $(CFLAGS) appl=fastosn dotfle=$(TD)fastosn.fle

$(TD)findpar.fle:   findpar.lgc
 $(CC) $(CFLAGS) appl=findpar dotfle=$(TD)findpar.fle

$(TD)l_kredit.fle:   l_kredit.lgc
 $(CC) $(CFLAGS) appl=l_kredit dotfle=$(TD)l_kredit.fle

$(TD)l_odbici.fle:   l_odbici.lgc
 $(CC) $(CFLAGS) appl=l_odbici dotfle=$(TD)l_odbici.fle

$(TD)l_potpli.fle:   l_potpli.lgc
 $(CC) $(CFLAGS) appl=l_potpli dotfle=$(TD)l_potpli.fle

$(TD)l_rekban.fle:   l_rekban.lgc
 $(CC) $(CFLAGS) appl=l_rekban dotfle=$(TD)l_rekban.fle

$(TD)l_uk_izn.fle:   l_uk_izn.lgc
 $(CC) $(CFLAGS) appl=l_uk_izn dotfle=$(TD)l_uk_izn.fle

$(TD)pl.fle:   pl.lgc
 $(CC) $(CFLAGS) appl=pl dotfle=$(TD)pl.fle

$(TD)pl_aobr.fle:   pl_aobr.lgc
 $(CC) $(CFLAGS) appl=pl_aobr dotfle=$(TD)pl_aobr.fle

$(TD)pl_arhiv.fle:   pl_arhiv.lgc
 $(CC) $(CFLAGS) appl=pl_arhiv dotfle=$(TD)pl_arhiv.fle

$(TD)pl_avirm.fle:   pl_avirm.lgc
 $(CC) $(CFLAGS) appl=pl_avirm dotfle=$(TD)pl_avirm.fle

$(TD)pl_choj.fle:   pl_choj.lgc
 $(CC) $(CFLAGS) appl=pl_choj dotfle=$(TD)pl_choj.fle

$(TD)pl_disk.fle:   pl_disk.lgc
 $(CC) $(CFLAGS) appl=pl_disk dotfle=$(TD)pl_disk.fle

$(TD)pl_dmds.fle:   pl_dmds.lgc
 $(CC) $(CFLAGS) appl=pl_dmds dotfle=$(TD)pl_dmds.fle

$(TD)pl_er.fle:   pl_er.lgc
 $(CC) $(CFLAGS) appl=pl_er dotfle=$(TD)pl_er.fle

$(TD)pl_fix.fle:   pl_fix.lgc
 $(CC) $(CFLAGS) appl=pl_fix dotfle=$(TD)pl_fix.fle

$(TD)pl_flds.fle:   pl_flds.lgc
 $(CC) $(CFLAGS) appl=pl_flds dotfle=$(TD)pl_flds.fle

$(TD)pl_fonds.fle:   pl_fonds.lgc
 $(CC) $(CFLAGS) appl=pl_fonds dotfle=$(TD)pl_fonds.fle

$(TD)pl_getos.fle:   pl_getos.lgc
 $(CC) $(CFLAGS) appl=pl_getos dotfle=$(TD)pl_getos.fle

$(TD)pl_getpr.fle:   pl_getpr.lgc
 $(CC) $(CFLAGS) appl=pl_getpr dotfle=$(TD)pl_getpr.fle

$(TD)pl_grkr.fle:   pl_grkr.lgc
 $(CC) $(CFLAGS) appl=pl_grkr dotfle=$(TD)pl_grkr.fle

$(TD)pl_gruid.fle:   pl_gruid.lgc
 $(CC) $(CFLAGS) appl=pl_gruid dotfle=$(TD)pl_gruid.fle

$(TD)pl_id.fle:   pl_id.lgc
 $(CC) $(CFLAGS) appl=pl_id dotfle=$(TD)pl_id.fle

$(TD)pl_id_mp.fle:   pl_id_mp.lgc
 $(CC) $(CFLAGS) appl=pl_id_mp dotfle=$(TD)pl_id_mp.fle

$(TD)pl_iizr.fle:   pl_iizr.lgc
 $(CC) $(CFLAGS) appl=pl_iizr dotfle=$(TD)pl_iizr.fle

$(TD)pl_iizr2.fle:   pl_iizr2.lgc
 $(CC) $(CFLAGS) appl=pl_iizr2 dotfle=$(TD)pl_iizr2.fle

$(TD)pl_ikred.fle:   pl_ikred.lgc
 $(CC) $(CFLAGS) appl=pl_ikred dotfle=$(TD)pl_ikred.fle

$(TD)pl_inior.fle:   pl_inior.lgc
 $(CC) $(CFLAGS) appl=pl_inior dotfle=$(TD)pl_inior.fle

$(TD)pl_inizr.fle:   pl_inizr.lgc
 $(CC) $(CFLAGS) appl=pl_inizr dotfle=$(TD)pl_inizr.fle

$(TD)pl_iorgs.fle:   pl_iorgs.lgc
 $(CC) $(CFLAGS) appl=pl_iorgs dotfle=$(TD)pl_iorgs.fle

$(TD)pl_iorgs2.fle:   pl_iorgs2.lgc
 $(CC) $(CFLAGS) appl=pl_iorgs2 dotfle=$(TD)pl_iorgs2.fle

$(TD)pl_ipor.fle:   pl_ipor.lgc
 $(CC) $(CFLAGS) appl=pl_ipor dotfle=$(TD)pl_ipor.fle

$(TD)pl_iradn.fle:   pl_iradn.lgc
 $(CC) $(CFLAGS) appl=pl_iradn dotfle=$(TD)pl_iradn.fle

$(TD)pl_ispld.fle:   pl_ispld.lgc
 $(CC) $(CFLAGS) appl=pl_ispld dotfle=$(TD)pl_ispld.fle

$(TD)pl_isplo.fle:   pl_isplo.lgc
 $(CC) $(CFLAGS) appl=pl_isplo dotfle=$(TD)pl_isplo.fle

$(TD)pl_ispls.fle:   pl_ispls.lgc
 $(CC) $(CFLAGS) appl=pl_ispls dotfle=$(TD)pl_ispls.fle

$(TD)pl_ispmj.fle:   pl_ispmj.lgc
 $(CC) $(CFLAGS) appl=pl_ispmj dotfle=$(TD)pl_ispmj.fle

$(TD)pl_istkr.fle:   pl_istkr.lgc
 $(CC) $(CFLAGS) appl=pl_istkr dotfle=$(TD)pl_istkr.fle

$(TD)pl_izvkr.fle:   pl_izvkr.lgc
 $(CC) $(CFLAGS) appl=pl_izvkr dotfle=$(TD)pl_izvkr.fle

$(TD)pl_keakt.fle:   pl_keakt.lgc
 $(CC) $(CFLAGS) appl=pl_keakt dotfle=$(TD)pl_keakt.fle

$(TD)pl_kebod.fle:   pl_kebod.lgc
 $(CC) $(CFLAGS) appl=pl_kebod dotfle=$(TD)pl_kebod.fle

$(TD)pl_keimp.fle:   pl_keimp.lgc
 $(CC) $(CFLAGS) appl=pl_keimp dotfle=$(TD)pl_keimp.fle

$(TD)pl_keola.fle:   pl_keola.lgc
 $(CC) $(CFLAGS) appl=pl_keola dotfle=$(TD)pl_keola.fle

$(TD)pl_keopc.fle:   pl_keopc.lgc
 $(CC) $(CFLAGS) appl=pl_keopc dotfle=$(TD)pl_keopc.fle

$(TD)pl_keorg.fle:   pl_keorg.lgc
 $(CC) $(CFLAGS) appl=pl_keorg dotfle=$(TD)pl_keorg.fle

$(TD)pl_kerm.fle:   pl_kerm.lgc
 $(CC) $(CFLAGS) appl=pl_kerm dotfle=$(TD)pl_kerm.fle

$(TD)pl_kesat.fle:   pl_kesat.lgc
 $(CC) $(CFLAGS) appl=pl_kesat dotfle=$(TD)pl_kesat.fle

$(TD)pl_kestz.fle:   pl_kestz.lgc
 $(CC) $(CFLAGS) appl=pl_kestz dotfle=$(TD)pl_kestz.fle

$(TD)pl_kezar.fle:   pl_kezar.lgc
 $(CC) $(CFLAGS) appl=pl_kezar dotfle=$(TD)pl_kezar.fle

$(TD)pl_klsql.fle:   pl_klsql.lgc
 $(CC) $(CFLAGS) appl=pl_klsql dotfle=$(TD)pl_klsql.fle

$(TD)pl_ler1.fle:   pl_ler1.lgc
 $(CC) $(CFLAGS) appl=pl_ler1 dotfle=$(TD)pl_ler1.fle

$(TD)pl_lgrid.fle:   pl_lgrid.lgc
 $(CC) $(CFLAGS) appl=pl_lgrid dotfle=$(TD)pl_lgrid.fle

$(TD)pl_lgrkr.fle:   pl_lgrkr.lgc
 $(CC) $(CFLAGS) appl=pl_lgrkr dotfle=$(TD)pl_lgrkr.fle

$(TD)pl_liip.fle:   pl_liip.lgc
 $(CC) $(CFLAGS) appl=pl_liip dotfle=$(TD)pl_liip.fle

$(TD)pl_lirad.fle:   pl_lirad.lgc
 $(CC) $(CFLAGS) appl=pl_lirad dotfle=$(TD)pl_lirad.fle

$(TD)pl_lismj.fle:   pl_lismj.lgc
 $(CC) $(CFLAGS) appl=pl_lismj dotfle=$(TD)pl_lismj.fle

$(TD)pl_lispl.fle:   pl_lispl.lgc
 $(CC) $(CFLAGS) appl=pl_lispl dotfle=$(TD)pl_lispl.fle

$(TD)pl_liste.fle:   pl_liste.lgc
 $(CC) $(CFLAGS) appl=pl_liste dotfle=$(TD)pl_liste.fle

$(TD)pl_lopri.fle:   pl_lopri.lgc
 $(CC) $(CFLAGS) appl=pl_lopri dotfle=$(TD)pl_lopri.fle

$(TD)pl_lpor.fle:   pl_lpor.lgc
 $(CC) $(CFLAGS) appl=pl_lpor dotfle=$(TD)pl_lpor.fle

$(TD)pl_lprro.fle:   pl_lprro.lgc
 $(CC) $(CFLAGS) appl=pl_lprro dotfle=$(TD)pl_lprro.fle

$(TD)pl_lvirm.fle:   pl_lvirm.lgc
 $(CC) $(CFLAGS) appl=pl_lvirm dotfle=$(TD)pl_lvirm.fle

$(TD)pl_lvro.fle:   pl_lvro.lgc
 $(CC) $(CFLAGS) appl=pl_lvro dotfle=$(TD)pl_lvro.fle

$(TD)pl_lvrs.fle:   pl_lvrs.lgc
 $(CC) $(CFLAGS) appl=pl_lvrs dotfle=$(TD)pl_lvrs.fle

$(TD)pl_lzpr.fle:   pl_lzpr.lgc
 $(CC) $(CFLAGS) appl=pl_lzpr dotfle=$(TD)pl_lzpr.fle

$(TD)pl_m4.fle:   pl_m4.lgc
 $(CC) $(CFLAGS) appl=pl_m4 dotfle=$(TD)pl_m4.fle

$(TD)pl_mds.fle:   pl_mds.lgc
 $(CC) $(CFLAGS) appl=pl_mds dotfle=$(TD)pl_mds.fle

$(TD)pl_minra.fle:   pl_minra.lgc
 $(CC) $(CFLAGS) appl=pl_minra dotfle=$(TD)pl_minra.fle

$(TD)pl_msgs.fle:   pl_msgs.lgc
 $(CC) $(CFLAGS) appl=pl_msgs dotfle=$(TD)pl_msgs.fle

$(TD)pl_naco.fle:   pl_naco.lgc
 $(CC) $(CFLAGS) appl=pl_naco dotfle=$(TD)pl_naco.fle

$(TD)pl_obrkr.fle:   pl_obrkr.lgc
 $(CC) $(CFLAGS) appl=pl_obrkr dotfle=$(TD)pl_obrkr.fle

$(TD)pl_obrnw.fle:   pl_obrnw.lgc
 $(CC) $(CFLAGS) appl=pl_obrnw dotfle=$(TD)pl_obrnw.fle

$(TD)pl_obros.fle:   pl_obros.lgc
 $(CC) $(CFLAGS) appl=pl_obros dotfle=$(TD)pl_obros.fle

$(TD)pl_obrpr.fle:   pl_obrpr.lgc
 $(CC) $(CFLAGS) appl=pl_obrpr dotfle=$(TD)pl_obrpr.fle

$(TD)pl_obrtm.fle:   pl_obrtm.lgc
 $(CC) $(CFLAGS) appl=pl_obrtm dotfle=$(TD)pl_obrtm.fle

$(TD)pl_obrz.fle:   pl_obrz.lgc
 $(CC) $(CFLAGS) appl=pl_obrz dotfle=$(TD)pl_obrz.fle

$(TD)pl_ojbrw.fle:   pl_ojbrw.lgc
 $(CC) $(CFLAGS) appl=pl_ojbrw dotfle=$(TD)pl_ojbrw.fle

$(TD)pl_pk.fle:   pl_pk.lgc
 $(CC) $(CFLAGS) appl=pl_pk dotfle=$(TD)pl_pk.fle

$(TD)pl_pk_go.fle:   pl_pk_go.lgc
 $(CC) $(CFLAGS) appl=pl_pk_go dotfle=$(TD)pl_pk_go.fle

$(TD)pl_plraz.fle:   pl_plraz.lgc
 $(CC) $(CFLAGS) appl=pl_plraz dotfle=$(TD)pl_plraz.fle

$(TD)pl_por.fle:   pl_por.lgc
 $(CC) $(CFLAGS) appl=pl_por dotfle=$(TD)pl_por.fle

$(TD)pl_prmr.fle:   pl_prmr.lgc
 $(CC) $(CFLAGS) appl=pl_prmr dotfle=$(TD)pl_prmr.fle

$(TD)pl_prpov.fle:   pl_prpov.lgc
 $(CC) $(CFLAGS) appl=pl_prpov dotfle=$(TD)pl_prpov.fle

$(TD)pl_rispa.fle:   pl_rispa.lgc
 $(CC) $(CFLAGS) appl=pl_rispa dotfle=$(TD)pl_rispa.fle

$(TD)pl_rispl.fle:   pl_rispl.lgc
 $(CC) $(CFLAGS) appl=pl_rispl dotfle=$(TD)pl_rispl.fle

$(TD)pl_rnup.fle:   pl_rnup.lgc
 $(CC) $(CFLAGS) appl=pl_rnup dotfle=$(TD)pl_rnup.fle

$(TD)pl_roisp.fle:   pl_roisp.lgc
 $(CC) $(CFLAGS) appl=pl_roisp dotfle=$(TD)pl_roisp.fle

$(TD)pl_stzar.fle:   pl_stzar.lgc
 $(CC) $(CFLAGS) appl=pl_stzar dotfle=$(TD)pl_stzar.fle

$(TD)pl_svikr.fle:   pl_svikr.lgc
 $(CC) $(CFLAGS) appl=pl_svikr dotfle=$(TD)pl_svikr.fle

$(TD)pl_virm.fle:   pl_virm.lgc
 $(CC) $(CFLAGS) appl=pl_virm dotfle=$(TD)pl_virm.fle

$(TD)pl_virmd.fle:   pl_virmd.lgc
 $(CC) $(CFLAGS) appl=pl_virmd dotfle=$(TD)pl_virmd.fle

$(TD)pl_virms.fle:   pl_virms.lgc
 $(CC) $(CFLAGS) appl=pl_virms dotfle=$(TD)pl_virms.fle

$(TD)pl_vispc.fle:   pl_vispc.lgc
 $(CC) $(CFLAGS) appl=pl_vispc dotfle=$(TD)pl_vispc.fle

$(TD)pl_vro.fle:   pl_vro.lgc
 $(CC) $(CFLAGS) appl=pl_vro dotfle=$(TD)pl_vro.fle

$(TD)pl_vrsat.fle:   pl_vrsat.lgc
 $(CC) $(CFLAGS) appl=pl_vrsat dotfle=$(TD)pl_vrsat.fle

$(TD)pl_zbrek.fle:   pl_zbrek.lgc
 $(CC) $(CFLAGS) appl=pl_zbrek dotfle=$(TD)pl_zbrek.fle

$(TD)pl_zim.fle:   pl_zim.lgc
 $(CC) $(CFLAGS) appl=pl_zim dotfle=$(TD)pl_zim.fle

$(TD)pl_zimtr.fle:   pl_zimtr.lgc
 $(CC) $(CFLAGS) appl=pl_zimtr dotfle=$(TD)pl_zimtr.fle

$(TD)ramp.fle:   ramp.lgc
 $(CC) $(CFLAGS) appl=ramp dotfle=$(TD)ramp.fle

$(TD)pldbutil.fle:   pldbutil.lgc
 $(CC) $(CFLAGS) appl=pldbutil dotfle=$(TD)pldbutil.fle
 $(CC) $(CFLAGS) appl=pldbutil dotfle=$(AD)pldbutil.fle
 @echo " " > pldbutil.ts

$(TD)pl_lovir.fle:   pl_lovir.lgc
 $(CC) $(CFLAGS) appl=pl_lovir dotfle=$(TD)pl_lovir.fle
 
$(TD)pl_mjizv.fle:   pl_mjizv.lgc
 $(CC) $(CFLAGS) appl=pl_mjizv dotfle=$(TD)pl_mjizv.fle

$(TD)pl_vrtro.fle:   pl_vrtro.lgc
 $(CC) $(CFLAGS) appl=pl_vrtro dotfle=$(TD)pl_vrtro.fle

$(TD)pl_lrbn.fle:   pl_lrbn.lgc
 $(CC) $(CFLAGS) appl=pl_lrbn dotfle=$(TD)pl_lrbn.fle

$(TD)pl_plista.fle:   pl_plista.lgc
 $(CC) $(CFLAGS) appl=pl_plista dotfle=$(TD)pl_plista.fle

$(TD)lisplpbz.fle:   lisplpbz.lgc
 $(CC) $(CFLAGS) appl=lisplpbz dotfle=$(TD)lisplpbz.fle

$(TD)pl_nalup.fle:   pl_nalup.lgc
 $(CC) $(CFLAGS) appl=pl_nalup dotfle=$(TD)pl_nalup.fle

$(TD)pl_a_dd.fle:   pl_a_dd.lgc
 $(CC) $(CFLAGS) appl=pl_a_dd dotfle=$(TD)pl_a_dd.fle

$(TD)l_listpbz.fle:   l_listpbz.lgc
 $(CC) $(CFLAGS) appl=l_listpbz dotfle=$(TD)l_listpbz.fle

$(TD)pltms_dd.fle:   pltms_dd.lgc
 $(CC) $(CFLAGS) appl=pltms_dd dotfle=$(TD)pltms_dd.fle

$(TD)l_altir.fle:   l_altir.lgc
 $(CC) $(CFLAGS) appl=l_altir dotfle=$(TD)l_altir.fle

$(TD)l_altik.fle:   l_altik.lgc
 $(CC) $(CFLAGS) appl=l_altik dotfle=$(TD)l_altik.fle

$(TD)l_ikred.fle:   l_ikred.lgc
 $(CC) $(CFLAGS) appl=l_ikred dotfle=$(TD)l_ikred.fle

$(TD)pl_rad1.fle:   pl_rad1.lgc
 $(CC) $(CFLAGS) appl=pl_rad1 dotfle=$(TD)pl_rad1.fle

$(TD)l_aakred.fle:   l_aakred.lgc
 $(CC) $(CFLAGS) appl=l_aakred dotfle=$(TD)l_aakred.fle

$(TD)l_allkred.fle:   l_allkred.lgc
 $(CC) $(CFLAGS) appl=l_allkred dotfle=$(TD)l_allkred.fle

$(TD)pl_lvp_h.fle:   pl_lvp_h.lgc
 $(CC) $(CFLAGS) appl=pl_lvp_h dotfle=$(TD)pl_lvp_h.fle

$(TD)pl_lm4.fle:   pl_lm4.lgc
 $(CC) $(CFLAGS) appl=pl_lm4 dotfle=$(TD)pl_lm4.fle

$(TD)netobruto.fle:   netobruto.lgc
 $(CC) $(CFLAGS) appl=netobruto dotfle=$(TD)netobruto.fle

$(TD)l_ist_kr.fle:   l_ist_kr.lgc
 $(CC) $(CFLAGS) appl=l_ist_kr dotfle=$(TD)l_ist_kr.fle

$(TD)m4regob.fle:   m4regob.lgc
 $(CC) $(CFLAGS) appl=m4regob dotfle=$(TD)m4regob.fle

$(TD)regbrM4.fle:   regbrM4.lgc
 $(CC) $(CFLAGS) appl=regbrM4 dotfle=$(TD)regbrM4.fle

$(TD)aazregos.fle:   aazregos.lgc
 $(CC) $(CFLAGS) appl=aazregos dotfle=$(TD)aazregos.fle

$(TD)aasregos.fle:   aasregos.lgc
 $(CC) $(CFLAGS) appl=aasregos dotfle=$(TD)aasregos.fle

$(TD)l_agerror.fle:   l_agerror.lgc
 $(CC) $(CFLAGS) appl=l_agerror dotfle=$(TD)l_agerror.fle

$(TD)pl_makeRS.fle:   pl_makeRS.lgc
 $(CC) $(CFLAGS) appl=pl_makeRS dotfle=$(TD)pl_makeRS.fle

$(TD)pl_fileRS.fle:   pl_fileRS.lgc
 $(CC) $(CFLAGS) appl=pl_fileRS dotfle=$(TD)pl_fileRS.fle

$(TD)pl_fileRSm.fle:   pl_fileRSm.lgc
 $(CC) $(CFLAGS) appl=pl_fileRSm dotfle=$(TD)pl_fileRSm.fle

$(TD)pl_kl3reg.fle:   pl_kl3reg.lgc
 $(CC) $(CFLAGS) appl=pl_kl3reg dotfle=$(TD)pl_kl3reg.fle

$(TD)pl_tuneRS.fle:   pl_tuneRS.lgc
 $(CC) $(CFLAGS) appl=pl_tuneRS dotfle=$(TD)pl_tuneRS.fle

$(TD)pl_spl.fle:   pl_spl.lgc
 $(CC) $(CFLAGS) appl=pl_spl dotfle=$(TD)pl_spl.fle

$(TD)pl_idx.fle:   pl_idx.lgc
 $(CC) $(CFLAGS) appl=pl_idx dotfle=$(TD)pl_idx.fle

$(TD)lpl_olak.fle:   lpl_olak.lgc
 $(CC) $(CFLAGS) appl=lpl_olak dotfle=$(TD)lpl_olak.fle

$(TD)id_pl.fle:   id_pl.lgc
 $(CC) $(CFLAGS) appl=id_pl dotfle=$(TD)id_pl.fle

$(TD)l_id_pl.fle:   l_id_pl.lgc
 $(CC) $(CFLAGS) appl=l_id_pl dotfle=$(TD)l_id_pl.fle

$(TD)l_id_pl_old.fle:   l_id_pl_old.lgc
 $(CC) $(CFLAGS) appl=l_id_pl_old dotfle=$(TD)l_id_pl_old.fle

$(TD)lid_pl_opc.fle:   lid_pl_opc.lgc
 $(CC) $(CFLAGS) appl=lid_pl_opc dotfle=$(TD)lid_pl_opc.fle

$(TD)lr_osnmio.fle:   lr_osnmio.lgc
 $(CC) $(CFLAGS) appl=lr_osnmio dotfle=$(TD)lr_osnmio.fle

$(TD)l_godobr.fle:   l_godobr.lgc
 $(CC) $(CFLAGS) appl=l_godobr dotfle=$(TD)l_godobr.fle

$(TD)l_go_opc.fle:   l_go_opc.lgc
 $(CC) $(CFLAGS) appl=l_go_opc dotfle=$(TD)l_go_opc.fle

$(TD)lrek_m4.fle:   lrek_m4.lgc
 $(CC) $(CFLAGS) appl=lrek_m4 dotfle=$(TD)lrek_m4.fle

$(TD)pl_fixbod.fle:   pl_fixbod.lgc
 $(CC) $(CFLAGS) appl=pl_fixbod dotfle=$(TD)pl_fixbod.fle

$(TD)m4_rekap.fle:   m4_rekap.lgc
 $(CC) $(CFLAGS) appl=m4_rekap dotfle=$(TD)m4_rekap.fle

$(TD)pl_godobr.fle:   pl_godobr.lgc
 $(CC) $(CFLAGS) appl=pl_godobr dotfle=$(TD)pl_godobr.fle

$(TD)pl_godobr2.fle:   pl_godobr2.lgc
 $(CC) $(CFLAGS) appl=pl_godobr2 dotfle=$(TD)pl_godobr2.fle

$(TD)pl_to_gk.fle:   pl_to_gk.lgc
 $(CC) $(CFLAGS) appl=pl_to_gk dotfle=$(TD)pl_to_gk.fle

$(TD)l_preg_mr.fle:   l_preg_mr.lgc
 $(CC) $(CFLAGS) appl=l_preg_mr dotfle=$(TD)l_preg_mr.fle

$(TD)sepa_vrop.fle:   sepa_vrop.lgc
 $(CC) $(CFLAGS) appl=sepa_vrop dotfle=$(TD)sepa_vrop.fle

$(TD)pbz_lvrad.fle:   pbz_lvrad.lgc
 $(CC) $(CFLAGS) appl=pbz_lvrad dotfle=$(TD)pbz_lvrad.fle

$(TD)godbruto.fle:   godbruto.lgc
 $(CC) $(CFLAGS) appl=godbruto dotfle=$(TD)godbruto.fle

$(TD)lpl_stzar.fle:   lpl_stzar.lgc
 $(CC) $(CFLAGS) appl=lpl_stzar dotfle=$(TD)lpl_stzar.fle

$(TD)pl_start.fle:   pl_start.lgc
 $(CC) $(CFLAGS) appl=pl_start dotfle=$(TD)pl_start.fle

$(TD)l_rad1g.fle:   l_rad1g.lgc
 $(CC) $(CFLAGS) appl=l_rad1g dotfle=$(TD)l_rad1g.fle

$(TD)periodicni.fle:   periodicni.lgc
 $(CC) $(CFLAGS) appl=periodicni dotfle=$(TD)periodicni.fle

$(TD)l_per_place.fle:   l_per_place.lgc
 $(CC) $(CFLAGS) appl=l_per_place dotfle=$(TD)l_per_place.fle

$(TD)l_gmz1_radn.fle:   l_gmz1_radn.lgc
 $(CC) $(CFLAGS) appl=l_gmz1_radn dotfle=$(TD)l_gmz1_radn.fle

$(TD)l_gmz2_radn.fle:   l_gmz2_radn.lgc
 $(CC) $(CFLAGS) appl=l_gmz2_radn dotfle=$(TD)l_gmz2_radn.fle

$(TD)aarekban.fle:   aarekban.lgc
 $(CC) $(CFLAGS) appl=aarekban dotfle=$(TD)aarekban.fle
 
$(TD)pl_history.fle:   pl_history.lgc
 $(CC) $(CFLAGS) appl=pl_history dotfle=$(TD)pl_history.fle

$(TD)l_gmz1_flukt.fle:   l_gmz1_flukt.lgc
 $(CC) $(CFLAGS) appl=l_gmz1_flukt dotfle=$(TD)l_gmz1_flukt.fle

$(TD)l_gmz2_flukt.fle:   l_gmz2_flukt.lgc
 $(CC) $(CFLAGS) appl=l_gmz2_flukt dotfle=$(TD)l_gmz2_flukt.fle

$(TD)iizara_nb.fle:   iizara_nb.lgc
 $(CC) $(CFLAGS) appl=iizara_nb dotfle=$(TD)iizara_nb.fle

$(TD)l_history.fle:   l_history.lgc
 $(CC) $(CFLAGS) appl=l_history dotfle=$(TD)l_history.fle

#- ***   AUTORSKI HONORARI   ***

$(TD)ah_dd.fle:   ah_dd.lgc
 $(CC) $(CFLAGS) appl=ah_dd dotfle=$(TD)ah_dd.fle
 $(CC) -w genddx ddfile=ah_dd
 $(CC) $(CFLAGS) appl=ah_ddx dotfle=$(TD)ah_dd.fle

$(TD)ah_radnici.fle:   ah_radnici.lgc
 $(CC) $(CFLAGS) appl=ah_radnici dotfle=$(TD)ah_radnici.fle

$(TD)ah_ugo.fle:   ah_ugo.lgc
 $(CC) $(CFLAGS) appl=ah_ugo dotfle=$(TD)ah_ugo.fle

$(TD)ah_koefs.fle:   ah_koefs.lgc
 $(CC) $(CFLAGS) appl=ah_koefs dotfle=$(TD)ah_koefs.fle

$(TD)ah_a_dd.fle:   ah_a_dd.lgc
 $(CC) $(CFLAGS) appl=ah_a_dd dotfle=$(TD)ah_a_dd.fle

$(TD)ah_calc.fle:   ah_calc.lgc
 $(CC) $(CFLAGS) appl=ah_calc dotfle=$(TD)ah_calc.fle

$(TD)ah_virm.fle:   ah_virm.lgc
 $(CC) $(CFLAGS) appl=ah_virm dotfle=$(TD)ah_virm.fle

$(TD)ah_virmd.fle:   ah_virmd.lgc
 $(CC) $(CFLAGS) appl=ah_virmd dotfle=$(TD)ah_virmd.fle

$(TD)hpah_virm.fle:   hpah_virm.lgc
 $(CC) $(CFLAGS) appl=hpah_virm dotfle=$(TD)hpah_virm.fle

$(TD)lah_kliste.fle:   lah_kliste.lgc
 $(CC) $(CFLAGS) appl=lah_kliste dotfle=$(TD)lah_kliste.fle

$(TD)ah_allarh.fle:   ah_allarh.lgc
 $(CC) $(CFLAGS) appl=ah_allarh dotfle=$(TD)ah_allarh.fle

$(TD)ah_idd.fle:   ah_idd.lgc
 $(CC) $(CFLAGS) appl=ah_idd dotfle=$(TD)ah_idd.fle

$(TD)lah_iddz.fle:   lah_iddz.lgc
 $(CC) $(CFLAGS) appl=lah_iddz dotfle=$(TD)lah_iddz.fle

$(TD)lah_listic.fle:   lah_listic.lgc
 $(CC) $(CFLAGS) appl=lah_listic dotfle=$(TD)lah_listic.fle

$(TD)lah_idd.fle:   lah_idd.lgc
 $(CC) $(CFLAGS) appl=lah_idd dotfle=$(TD)lah_idd.fle

$(TD)lah_iddgod.fle:   lah_iddgod.lgc
 $(CC) $(CFLAGS) appl=lah_iddgod dotfle=$(TD)lah_iddgod.fle

$(TD)pl_ident.fle:   pl_ident.lgc
 $(CC) $(CFLAGS) appl=pl_ident dotfle=$(TD)pl_ident.fle

$(TD)ah_fileRSm.fle:   ah_fileRSm.lgc
 $(CC) $(CFLAGS) appl=ah_fileRSm dotfle=$(TD)ah_fileRSm.fle

$(TD)obrz_id1.fle:   obrz_id1.lgc
 $(CC) $(CFLAGS) appl=obrz_id1 dotfle=$(TD)obrz_id1.fle

$(TD)bol_odjel.fle:   bol_odjel.lgc
 $(CC) $(CFLAGS) appl=bol_odjel dotfle=$(TD)bol_odjel.fle

$(TD)l_bol_odjel.fle:   l_bol_odjel.lgc
 $(CC) $(CFLAGS) appl=l_bol_odjel dotfle=$(TD)l_bol_odjel.fle

$(TD)reiizara.fle:   reiizara.lgc
 $(CC) $(CFLAGS) appl=reiizara dotfle=$(TD)reiizara.fle

$(TD)lpl_harac.fle:   lpl_harac.lgc
 $(CC) $(CFLAGS) appl=lpl_harac dotfle=$(TD)lpl_harac.fle

$(TD)lah_harac.fle:   lah_harac.lgc
 $(CC) $(CFLAGS) appl=lah_harac dotfle=$(TD)lah_harac.fle

$(TD)ah_troskovi.fle:   ah_troskovi.lgc
 $(CC) $(CFLAGS) appl=ah_troskovi dotfle=$(TD)ah_troskovi.fle

$(TD)l_harac.fle:   l_harac.lgc
 $(CC) $(CFLAGS) appl=l_harac dotfle=$(TD)l_harac.fle

$(TD)aa_lprro.fle:   aa_lprro.lgc
 $(CC) $(CFLAGS) appl=aa_lprro dotfle=$(TD)aa_lprro.fle

$(TD)pl_aalzpr.fle:   pl_aalzpr.lgc
 $(CC) $(CFLAGS) appl=pl_aalzpr dotfle=$(TD)pl_aalzpr.fle

$(TD)lpl_tipor.fle:   lpl_tipor.lgc
 $(CC) $(CFLAGS) appl=lpl_tipor dotfle=$(TD)lpl_tipor.fle

$(TD)banke.fle:  banke.lgc
    $(CC) $(CFLAGS) appl=banke dotfle=$(TD)banke.fle

$(TD)l_xii_kartica.fle:  l_xii_kartica.lgc
    $(CC) $(CFLAGS) appl=l_xii_kartica dotfle=$(TD)l_xii_kartica.fle

$(TD)pl_storno.fle:  pl_storno.lgc
    $(CC) $(CFLAGS) appl=pl_storno dotfle=$(TD)pl_storno.fle

$(TD)shknj_ah.fle:   shknj_ah.lgc
 $(CC) $(CFLAGS) appl=shknj_ah dotfle=$(TD)shknj_ah.fle

$(TD)go_olakmax.fle:   go_olakmax.lgc
 $(CC) $(CFLAGS) appl=go_olakmax dotfle=$(TD)go_olakmax.fle

$(TD)pl_klista5.fle:   pl_klista5.lgc
 $(CC) $(CFLAGS) appl=pl_klista5 dotfle=$(TD)pl_klista5.fle

$(TD)l_iiz_joppd.fle:   l_iiz_joppd.lgc
 $(CC) $(CFLAGS) appl=l_iiz_joppd dotfle=$(TD)l_iiz_joppd.fle

$(TD)pl_rev.fle:  pl_rev.lgc
    $(CC) $(CFLAGS) appl=pl_rev dotfle=$(TD)pl_rev.fle
    COPY pl_rev.lgc $(YTD)pl_rev.lgc

